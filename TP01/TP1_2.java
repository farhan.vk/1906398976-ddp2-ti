import java.util.Scanner;
public class TP1_2 {
    public static void main(String[] args) {
        // TODO: Kerjakan Soal 2 disini!
        System.out.println("Selamat datang di DDP2");
        Scanner inp = new Scanner(System.in);
        System.out.print("Nama fakultas apa yang anda ingin ketahui?: ");
        String fakultas = inp.nextLine();
        fakultas = fakultas.toUpperCase();
        switch(fakultas){
            case "FK":
                System.out.println("Fakultas Kedokteran");
                break;
            case "FKG":
                System.out.println("Fakultas Kedokteran Gigi");
                break;
            case "FEB":
                System.out.println("Fakultas Ekonomi dan Bisnis");
                break;
            case "FH":
                System.out.println("Fakultas Hukum");
                break;
            case "FF":
                System.out.println("Fakultas Farmasi");
                break;
            case "FKM":
                System.out.println("Fakultas Kesehatan Masyarakat");
                break;
            case "FIK":
                System.out.println("Fakultas Ilmu Keperawatan");
                break;
            case "FMIPA":
                System.out.println("Fakultas Matematika dan Ilmu Pengetahuan Alam");
                break;
            case "FT":
                System.out.println("Fakultas Teknik");
                break;
            case "FASILKOM":
                System.out.println("Fakultas Ilmu Komputer");
                break;
            case "FIB":
                System.out.println("Fakultas Ilmu Pengetahuan Budaya");
                break;
            case "FPSI":
                System.out.println("Fakultas Psikologi");
                break;
            case "FISIP":
                System.out.println("Fakultas Ilmu Sosial dan Ilmu Politik");
                break;
            case "FIA":
                System.out.println("Fakultas Ilmu Administrasi");
                break;
            default:
                System.out.println("Fakultas tidak ditemukan");
        }
        inp.close();
    }
}
