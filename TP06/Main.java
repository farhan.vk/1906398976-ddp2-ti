import java.util.Scanner;
public class Main {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        scan.nextLine();
        Karyawan [] listKaryawan = new Karyawan[n];

        for(int i=0;i<n;i++){
            String input = scan.nextLine();
            String[] parts = input.split(" ");
            String nama = parts[0];
            int umur = Integer.parseInt(parts[1]);
            int lamaBekerja = Integer.parseInt(parts[2]);
            listKaryawan[i]= new Karyawan(nama, umur, lamaBekerja);
        }
        
        
        System.out.println("Rata-rata gaji karyawan adalah " + String.format("%.2f", rerataGaji(listKaryawan)));
        System.out.println("Karyawan dengan gaji tertinggi adalah " + gajiTertinggi(listKaryawan));
        System.out.println("Karyawan dengan gaji terendah adalah " + gajiTerendah(listKaryawan));
        scan.close();
    }

    //TODO
    public static double rerataGaji(Karyawan[] listKaryawan) {
        double rerata = 0;
        for(int i=0;i<listKaryawan.length; i++) {
            rerata += listKaryawan[i].getGaji();
        }
        rerata = rerata/listKaryawan.length;
        return rerata;
    }
    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTerendah(Karyawan[] listKaryawan) {
        double minValue = listKaryawan[0].getGaji();
        int indexOfMinValue = 0;
        for(int i = 1; i < listKaryawan.length; i++) {
        if(listKaryawan[i].getGaji() < minValue) {
            indexOfMinValue = i;
            minValue = listKaryawan[i].getGaji();
            }
        }
        return listKaryawan[indexOfMinValue].getNama();
    }

    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTertinggi(Karyawan[] listKaryawan) {
        double maxValue = listKaryawan[0].getGaji();
        int indexOfMaxValue = 0;
        for(int i = 1; i < listKaryawan.length; i++) {
        if(listKaryawan[i].getGaji() > maxValue) {
            indexOfMaxValue = i;
            maxValue = listKaryawan[i].getGaji();
            }
        }
        return listKaryawan[indexOfMaxValue].getNama();
    }

}
