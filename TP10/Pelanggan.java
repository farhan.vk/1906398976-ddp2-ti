public class Pelanggan extends Manusia{


    public Pelanggan(String nama, int uang){
        super(nama, uang);
    }

    public void setNama(String nama){
        this.nama = nama;
    }
    public String getNama(){
        return this.nama;
    }
    public String getTipe(){
        return "Pelanggan" + " " + this.nama;
    }

    @Override
    public String bicara(){
        return String.format("Halo, saya %s. Uang saya adalah %d.", nama, uang);
    }

    public String membeli(){
        return nama + " membeli makanan dan minuman di kedai VoidMain.";
    }
}
