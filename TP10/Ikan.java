public class Ikan extends Hewan{
    boolean isBeracun;

    public Ikan(String nama, String spesies, boolean isBeracun){
        super(nama, spesies);
        this.isBeracun = isBeracun;
    }

    public void setNama(String nama){
        this.nama = nama;
    }
    public String getNama(){
        return this.nama;
    }
    public String getTipe(){
        return "Ikan"  + " " + this.spesies;
    }

    @Override
    public String bersuara(){
        if(isBeracun){
            return String.format("Blub blub blub blub. Blub. (Halo, saya %s. Saya ikan yang beracun).", nama);
        }else{
            return String.format("Blub blub blub blub. Blub. (Halo, saya %s. Saya ikan yang tidak beracun).", nama);
        }
    }

    public String bernafas(){
        return nama + " bernafas dengan insang.";
    }
    public String bergerak(){
        return nama + " bergerak dengan cara berenang.";
    }

}
