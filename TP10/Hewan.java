public abstract class Hewan implements Makhluk{
    String nama;
    String spesies;

    public Hewan(String nama, String spesies){
        this.nama = nama;
        this.spesies = spesies;
    }

    public void setNama(String nama){
        this.nama = nama;
    }
    public String getNama(){
        return this.nama;
    }

    abstract String bersuara();

    public String toString(){
        return this.bersuara();
    }
}
