public abstract class Manusia implements Makhluk{
    String nama;
    int uang;

    public Manusia(String nama, int uang){
        this.nama = nama;
        this.uang = uang;
    }
    public void setNama(String nama){
        this.nama = nama;
    }
    public String getNama(){
        return this.nama;
    }

    public String bernafas(){
        return nama + " bernafas dengan paru-paru.";
    }
    public String bergerak(){
        return nama + " bergerak dengan cara berjalan.";
    }

    abstract String bicara();

    public String toString(){
        return this.bicara();
    }
}
