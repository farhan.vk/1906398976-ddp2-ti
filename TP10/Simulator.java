import java.util.Scanner;
import java.util.ArrayList;

public class Simulator {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        String kalimat;
        String nama;
        String panggil;
        String nama2;
        String perintah;
        boolean ada;

        ArrayList<Makhluk> list = new ArrayList<Makhluk>();

        Manusia yoga = new Pegawai("Yoga satu", 100000, "master");
        Manusia yoga2 = new Pegawai("Yoga dua", 200000, "master");
        Manusia bujang = new Pelanggan("Bujang", 100000);
        Hewan betta = new Ikan("Betta", "Betta Splendens", false);
        Hewan burhan = new BurungHantu("Burhan", "Bubo Scandiacus");
        Hewan kerapu = new Ikan("Kerapu Batik", "Epinephelus Polyphekadion", false);
        Hewan ikanTerbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", false);

        list.add(yoga);
        list.add(yoga2);
        list.add(bujang);
        list.add(betta);
        list.add(burhan);
        list.add(kerapu);
        list.add(ikanTerbang);

        do{
            System.out.print("Silakan masukkan perintah: ");
            kalimat = input.nextLine();

            try{
                int index = kalimat.lastIndexOf(" ");
                nama = kalimat.substring(0, index).toLowerCase();
                perintah = kalimat.substring(index+1).toLowerCase();

                int index2 = kalimat.indexOf(" ");
                panggil = kalimat.substring(0, index2).toLowerCase();
                nama2 = kalimat.substring(index2+1).toLowerCase();
            }catch(StringIndexOutOfBoundsException e){
                System.out.println("Maaf, perintah tidak ditemukan!");
                continue;
            }

            //mengatasi ambiguitas pemanggilan nama
            int counter = 0;
            for (Makhluk obj : list){
                if(obj.getNama().toLowerCase().contains(nama)){
                    counter++;
                    if(counter>1){
                        break;
                    }
                }
            }
            if(counter>1){
                System.out.println("Maaf, terdapat lebih dari satu makhluk yang memiliki nama tersebut. Harap memasukkan nama lebih spesifik.");
                continue;
            }

            //perintah panggil
            if(panggil.equals("panggil")){
                ada = false;
                for (Makhluk obj : list){
                    if(obj.getNama().toLowerCase().contains(nama2)){
                        System.out.println(obj);
                        ada = true;
                    }
                }
                if(!ada){
                    System.out.println(String.format("Maaf, %s tidak pernah melewati/mampir di kedai.", nama2));
                }
                continue;
            }

            ada = false;
            for (Makhluk obj : list){
                if(obj.getNama().toLowerCase().contains(nama)){
                    if(perintah.equals("bernafas")){
                        System.out.println(obj.bernafas());
                    }
                    else if(perintah.equals("bergerak")){
                        System.out.println(obj.bergerak());
                    }
                    else if(perintah.equals("bicara")){
                        if (obj instanceof Manusia) {
                            Manusia manusia = (Manusia) obj;
                            System.out.println(manusia.bicara());
                        }else{
                            System.out.println(String.format("Maaf, %s tidak bisa %s", obj.getTipe(), perintah));
                        }
                    }
                    else if(perintah.equals("bekerja")){
                        if (obj instanceof Pegawai) {
                            Pegawai pegawai = (Pegawai) obj;
                            System.out.println(pegawai.bekerja());
                        }else{
                            System.out.println(String.format("Maaf, %s tidak bisa %s", obj.getTipe(), perintah));
                        }
                    }
                    else if(perintah.equals("beli")){
                        if (obj instanceof Pelanggan) {
                            Pelanggan pelanggan = (Pelanggan) obj;
                            System.out.println(pelanggan.membeli());
                        }else{
                            System.out.println(String.format("Maaf, %s tidak bisa %s", obj.getTipe(), perintah));
                        }
                    }
                    else if(perintah.equals("bersuara")){
                        if (obj instanceof Hewan) {
                            Hewan hewan = (Hewan) obj;
                            System.out.println(hewan.bersuara());
                        }else{
                            System.out.println(String.format("Maaf, %s tidak bisa %s", obj.getTipe(), perintah));
                        }
                    }
                    else if(perintah.equals("libur")){
                        if (obj instanceof PegawaiSpesial) {
                            PegawaiSpesial pegawai = (PegawaiSpesial) obj;
                            System.out.println(pegawai.libur());
                        }else{
                            System.out.println(String.format("Maaf, %s tidak bisa %s", obj.getTipe(), perintah));
                        }
                    }
                    else if(perintah.equals("terbang")){
                        if (obj instanceof IkanSpesial) {
                            IkanSpesial ikan = (IkanSpesial) obj;
                            System.out.println(ikan.terbang());
                        }else{
                            System.out.println(String.format("Maaf, %s tidak bisa %s", obj.getTipe(), perintah));
                        }
                    }
                    else{
                        System.out.println(String.format("Maaf, %s tidak bisa %s", obj.getTipe(), perintah));
                    }
                    ada = true;
                    break;
                }
            }
            if(!ada){
                System.out.println("Maaf, tidak ada makhluk bernama " + nama);
            }


        } while(!kalimat.equals("selesai"));
        System.out.println("Sampai jumpa!");

        input.close();

    }
}
