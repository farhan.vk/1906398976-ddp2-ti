public interface Makhluk{
    public String bernafas();
    public String bergerak();
    public String getNama();
    public String getTipe();
    public String toString();
}