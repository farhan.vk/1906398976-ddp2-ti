public class PegawaiSpesial extends Manusia implements bisaLibur {
    String levelKeahlian;

    public PegawaiSpesial(String nama, int uang, String levelKeahlian){
        super(nama, uang);
        this.levelKeahlian = levelKeahlian;
    }

    public void setNama(String nama){
        this.nama = nama;
    }
    public String getNama(){
        return this.nama;
    }
    public String getTipe(){
        return "Pegawai"  + " " + this.nama;
    }

    @Override
    public String bicara(){
        return String.format("Halo, saya %s. Uang saya adalah %d, dan level keahlian saya adalah %s. Saya memiliki privilege yaitu bisa libur.", nama, uang, levelKeahlian);
    }

    public String bekerja(){
        return nama + " bekerja di kedai VoidMain";
    }

    public String libur(){
        return nama + " sedang berlibur ke Akihabara.";
    }
}
