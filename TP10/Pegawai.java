public class Pegawai extends Manusia{
    String levelKeahlian;

    public Pegawai(String nama, int uang, String levelKeahlian){
        super(nama, uang);
        this.levelKeahlian = levelKeahlian;
    }

    public void setNama(String nama){
        this.nama = nama;
    }
    public String getNama(){
        return this.nama;
    }
    public String getTipe(){
        return "Pegawai" + " " + this.nama;
    }

    @Override
    public String bicara(){
        return String.format("Halo, saya %s. Uang saya adalah %d, dan level keahlian saya adalah %s.", nama, uang, levelKeahlian);
    }

    public String bekerja(){
        return nama + " bekerja di kedai VoidMain";
    }
}
