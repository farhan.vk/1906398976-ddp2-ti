public class BurungHantu extends Hewan {
    
    public BurungHantu(String nama, String spesies){
        super(nama, spesies);
    }

    public void setNama(String nama){
        this.nama = nama;
    }
    public String getNama(){
        return this.nama;
    }
    public String getTipe(){
        return "Burung Hantu"  + " " + this.spesies;
    }

    @Override
    public String bersuara(){
        return String.format("Hooooh hoooooooh.(Halo, saya %s. Saya adalah burung hantu).", nama);
    }

    public String bernafas(){
        return nama + " bernafas dengan paru-paru.";
    }
    public String bergerak(){
        return nama + " bergerak dengan cara terbang.";
    }
}
