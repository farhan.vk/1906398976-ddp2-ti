import java.util.Scanner;
public class TP2_3 {
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);

        String hewan;
        int jumlah;
        double kecepatan;
        boolean izin = false;

        System.out.println("Selamat datang di desa Dedepedua");
        System.out.println("Apa hewan kesukaan Kepala Desa?");
        hewan = in.nextLine();
        if(hewan.equals("burung hantu")){
            System.out.println("Berapa jumlah harimau di desa Dedepedua?");
            jumlah = in.nextInt();
            if(jumlah==8 || jumlah==10 || jumlah==18){
                System.out.println("Berapa kecepatan harimau di desa Dedepedua?");
                kecepatan = in.nextDouble();
                if(kecepatan > 100 && kecepatan < 120){
                    System.out.println("Selamat kamu boleh masuk");
                    izin = true;
                }
            }
        }
        if(!izin){
            System.out.println("Kamu dilarang masuk");
        }
        in.close();
    }
}
